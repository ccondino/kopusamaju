<?php

namespace App\Models;

class EmployeeLeaveTypeSetting extends \Eloquent
{
    protected $fillable = [];
    public $guarded = ['id'];
    public $table = 'employee_leave_type_settings';

    public function leave_type()
    {
        return $this->belongsTo(Leavetype::class, 'leave_type_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employeeID', 'employeeID');
    }
}
