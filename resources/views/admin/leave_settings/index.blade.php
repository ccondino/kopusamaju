@extends('admin.adminlayouts.adminlayout')

@section('head')
	<!-- BEGIN PAGE LEVEL STYLES -->
	{!! HTML::style("assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css") !!}
	<!-- END PAGE LEVEL STYLES -->

@stop

@section('mainarea')

  <!-- BEGIN PAGE HEADER-->
  <h3 class="page-title">
    {{$pageTitle}}
  </h3><!-- page-title -->

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <i class="fa fa-home"></i>
        <a href="{{route('admin.dashboard.index')}}">Home</a>
        <i class="fa fa-angle-right"></i>
      </li>
      <li>
        <a href="#">Leave Settings</a>
        <i class="fa "></i>
      </li>
    </ul>
  </div><!-- .page-bar -->

  <div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        Employees Leaves
      </div>
    </div>
  </div>

@endsection